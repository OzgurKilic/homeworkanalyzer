package tr.edu.mu.ceng.homeworkanalyzer;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import tr.edu.mu.ceng.homeworkanalyzer.Course;

public class TestCourse {
	
	private Course course;
	private FileFilter sourceFilter;
	private FileFilter executableFilter;

	@Before
	public void init() throws IOException {
		course = new Course("ceng1004");
		course.readStudents("Students");
		course.cloneRepositories("lab");
		//course.cloneRepositories("r#");
		sourceFilter = new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return (pathname.getName().toLowerCase().endsWith(".java") && !pathname.isDirectory()) ||
						pathname.isDirectory();
			}
		};
		executableFilter  = new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return (pathname.getName().toLowerCase().endsWith(".class") && !pathname.isDirectory()) ||
						pathname.isDirectory();
			}
		};
	}
		    
		@Test
		public void testCeng1004Lab1() throws IOException {

			try {
				course.printContents("lab1", sourceFilter);
				//Compiler compiler = new JavaFileCompiler();
				//Executer executer = new JavaFileExecuter();
				//course.compileAndRunContents("lab1", compiler, executer, null, sourceFilter, executableFilter);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	

	
}
