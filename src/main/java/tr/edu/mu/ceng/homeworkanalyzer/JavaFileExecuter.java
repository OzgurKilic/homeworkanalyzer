package tr.edu.mu.ceng.homeworkanalyzer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JavaFileExecuter implements Executer {

	Logger logger = LoggerFactory.getLogger(JavaFileExecuter.class);



	public String execute(File rootFolder, String executable, String[] args) throws IOException {

		StringBuffer result = new StringBuffer();

		
		ProcessBuilder processBuilder;
		List<String> command = buildArguments(executable, args);
		result.append(command + "\n");
		processBuilder = new ProcessBuilder(command);
		processBuilder.directory(rootFolder);

		final Process pr = processBuilder.start();
		
		Thread t = new Thread(new Runnable(){

			@Override
			public void run() {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					logger.error("Error during sleeping thread");
				}
				
				pr.destroy();
				
				logger.debug("After 10 seconds process was destroyed.");
			}
			
		});
		t.start();
		
		BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));

		String line = null;

		while ((line = input.readLine()) != null) {
			result.append(line+"\n");
			logger.debug(line);
		}

		try {
			int exitVal = pr.waitFor();
			logger.debug("Exited with error code " + exitVal);
		} catch (InterruptedException e) {
			logger.error("Error while waiting for process", e);
		}

		return result.toString();

	}

	private List<String> buildArguments(String filename, String[] args) {
		List<String> items = new ArrayList<>();
		items.add("java");
		items.add(filename);
		if (args != null) {
			for (String str : args) {
				items.add(str);
			}
		}
		logger.debug("Executing: " + items);
		return items;
	}
}
