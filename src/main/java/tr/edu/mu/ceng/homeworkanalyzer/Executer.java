package tr.edu.mu.ceng.homeworkanalyzer;

import java.io.File;
import java.io.IOException;

public interface Executer {

	public String execute(File rootFolder, String executable, String[] args) throws IOException;

}