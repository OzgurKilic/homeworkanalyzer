package tr.edu.mu.ceng.homeworkanalyzer;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JavaFileCompiler implements Compiler{

	Logger logger = LoggerFactory.getLogger(JavaFileCompiler.class);
	
	@Override
	public List<String> compile(File[] files) throws IOException{
		List<String> errors = new ArrayList<>();
		
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
		DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
		
		Iterable<? extends JavaFileObject> compilationUnits1 = fileManager
				.getJavaFileObjectsFromFiles(Arrays.asList(files));
		compiler.getTask(null, fileManager, diagnostics, null, null, compilationUnits1).call();

		for (Diagnostic diagnostic : diagnostics.getDiagnostics()) {
			
			String err = String.format("Error on line %d in %s\t%s%n", diagnostic.getLineNumber(),
					((JavaFileObject)diagnostic.getSource()).toUri(), diagnostic.getMessage(new Locale("en")));
			errors.add(err);
			logger.debug(err);
		}

		fileManager.close();
		
		return errors;
		
	}



	@Override
	public List<String> compile(File file) throws IOException {
		File[] files = {file};
		logger.debug("Compiling: "+ file.getName());
		return compile(files);
	}
}
