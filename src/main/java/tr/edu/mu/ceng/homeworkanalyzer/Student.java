package tr.edu.mu.ceng.homeworkanalyzer;

public class Student {
	private final String id;
	private StudentRepository repo;

	public Student(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public StudentRepository getRepo() {
		return repo;
	}

	public void setRepo(StudentRepository repo) {
		this.repo = repo;
	}
	
	
}
