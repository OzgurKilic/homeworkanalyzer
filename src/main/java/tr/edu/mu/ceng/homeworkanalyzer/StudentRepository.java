package tr.edu.mu.ceng.homeworkanalyzer;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StudentRepository {

	Logger logger = LoggerFactory.getLogger(StudentRepository.class);
	
	private final String studentId;
	
	private final String repo;
	
	private final File rootFolder;

	public StudentRepository(String id, File rootFolder, String repoName) {
		super();
		this.studentId = id;
		this.rootFolder = rootFolder;
		this.repo = "https://U"+id+"@bitbucket.org/U"+id+"/"+repoName+".git";
		logger.debug("Repository URL for " + id + " : " + repo);
	}
	
	public void cloneRepository() throws IOException, InvalidRemoteException, TransportException, GitAPIException{
		File studentFolder = new File(rootFolder.getPath()+"/U"+studentId);
		studentFolder.mkdir();
		  // then clone
		  try (Git result = Git.cloneRepository()
		            .setURI(repo)
		            .setDirectory(studentFolder)
		            .setCredentialsProvider(new UsernamePasswordCredentialsProvider( "OzgurKilic", "abc1234d" ))
		            .call()) {
		        // Important to close the repo after being used
			  logger.debug("Having repository: " + result.getRepository().getDirectory());
		  }
		
	}
	
	public void checkFolder(String folderPath, List<String> requiredFiles){
		File f = new File(folderPath);
		String[] paths = f.list();
		Set<String> items = new HashSet<>();
		for (String path : paths){
			items.add(path);
		}
		for (String reqFile: requiredFiles){
			if (!items.contains(reqFile)){
				logger.debug(reqFile + " is missing for student " + studentId);
			}
		}
	}
	
	public File getFolder(String targetFolderName){
		return new File(rootFolder.getPath()+"/U"+studentId+"/"+targetFolderName);
	}
	
	public File[] getContents(String targetFolderName, FileFilter filter){
		File targetFolder = new File(rootFolder.getPath()+"/U"+studentId+"/"+targetFolderName);
		return targetFolder.listFiles(filter);		
	}

	public List<File> getContentsRecursively(String targetFolderName, FileFilter filter){
		File targetFolder = new File(rootFolder.getPath()+"/U"+studentId+"/"+targetFolderName);
		return getContentsRecursively(targetFolder, filter);		
	}

	public List<File> getContentsRecursively(File folder, FileFilter filter){
		List<File> javaFiles= new ArrayList<>();
		File[] files = folder.listFiles(filter);
		for (File file: files){
			if (file.isDirectory()) {
				javaFiles.addAll(getContentsRecursively(file,  filter));
			}else{
				javaFiles.add(file);
			}
		}
		return 	javaFiles;	
	}
	
	
	public void copyFiles(String targetFolderName, File[] cpFiles) {
		for (File file : cpFiles){
			Path targetPath = Paths.get(rootFolder.getPath()+"/U"+studentId+"/"+targetFolderName+"/"+file.getName());
			Path sourcePath = file.toPath();
			try {
				Files.copy(sourcePath, targetPath, java.nio.file.StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				logger.error("Cannot copy" + file.getName(), e);
			}
		}
	}

	public File getRootFolder(String targetFolderName) {
		return 	new File(rootFolder.getPath()+"/U"+studentId+"/"+targetFolderName);
	}

	public List<String> getExecutablesRecursively(File rootFolder, FileFilter filterExecutables) {
		return getExecutablesRecursively(rootFolder, filterExecutables,"");
	}

	public List<String> getExecutablesRecursively(File rootFolder, FileFilter filterExecutables, String packageName) {
		List<String> executables= new ArrayList<>();
		File[] files = rootFolder.listFiles(filterExecutables);
		for (File file: files){
			if (file.isDirectory()) {
				executables.addAll(getExecutablesRecursively(file,  filterExecutables, packageName+file.getName()+"."));
			}else{
				executables.add(packageName + file.getName().substring(0, file.getName().indexOf('.')));
			}
		}
		return 	executables;	
	
	}	
}
