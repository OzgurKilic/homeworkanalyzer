package tr.edu.mu.ceng.homeworkanalyzer;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		if (args.length > 0){
			if (args[0].equals("-list")){
				if (args.length > 1){
					String repo = args[1];
					if (args.length > 2){
						String folder = args[2];
				    	Course course = new Course("cengXXXX");
				    	course.readStudents("Students");
				    	course.cloneRepositories(repo);
				    	FileFilter filter = new FileFilter() {
				            @Override
				            public boolean accept(File pathname) {
				                return true;
				            }
				        };
				        course.printContents(folder, filter);
					}else{
						usage();
					}
				}else{
					usage();
				}
			}else if (args[0].equals("-help")){
				usage();
			}
		}

	}
	
	public static void usage(){
		System.out.println("java -jar <jarfile> -list <reponame> <foldername>");
	}

}
