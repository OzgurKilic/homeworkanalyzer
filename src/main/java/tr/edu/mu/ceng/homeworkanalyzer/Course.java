package tr.edu.mu.ceng.homeworkanalyzer;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Course {

	String code;
	List<Student> students = new ArrayList<>();

	Logger logger = LoggerFactory.getLogger(Course.class);

	File rootFolder;

	public Course(String courseCode) throws IOException {
		code = courseCode;
		rootFolder = File.createTempFile(courseCode, "");
		rootFolder.delete();
	}

	public void readStudents(String filename) throws FileNotFoundException {
		Scanner scanner = new Scanner(new FileReader(filename));

		try {
			while (scanner.hasNext()) {
				String stuId = scanner.next();
				logger.debug("processing Student: " + stuId);
				Student st = new Student(stuId);
				students.add(st);
			}
		} finally {
			scanner.close();
		}
	}

	public void cloneRepositories(String repoName) {
		for (Student st : students) {
			String repositoryName = repoName.replaceFirst("#", st.getId());
			StudentRepository repo = new StudentRepository(st.getId(), rootFolder, repositoryName);
			try {
				repo.cloneRepository();
				st.setRepo(repo);
			} catch (IOException | GitAPIException e) {
				logger.error("Error in cloning repo for student " + st.getId(), e);

			}

		}
	}

	public void analyzeContents(String targetFolder, List<String> requiredFiles) {

	}

	public void copyFiles(String targetFolderName, File[] cpFiles) {
		for (Student st : students) {
			if (st.getRepo() == null) {
				logger.debug(st.getId() + " has no repository");
			} else {
				FileFilter filter = new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						return true;
					}
				};
				File[] files = st.getRepo().getContents(targetFolderName, filter);
				if (files != null) {
					st.getRepo().copyFiles(targetFolderName, cpFiles);
				} else {
					logger.debug(st.getId() + " does not have the directory " + targetFolderName);
				}
			}
		}
	}

	public void printContents(String targetFolderName, FileFilter filter) throws IOException {
		Formatter formatter = null;
		try {
			formatter = new Formatter(new FileWriter(targetFolderName + "_Contents.txt"));
			for (Student st : students) {
				formatter.format(st.getId());
				if (st.getRepo() == null) {
					formatter.format("\tNO REPOSITORY\n");
				} else {
					File folder = st.getRepo().getFolder(targetFolderName);
					printContents(folder, filter, formatter, "");	
					formatter.format("\n");
				}
			}
		} finally {
			if (formatter != null) {
				formatter.close();
			}
		}
	}

	private void printContents(File folder, FileFilter filter, Formatter formatter,  String parent) {
		File[] files = folder.listFiles(filter);
		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					printContents(file, filter, formatter,  parent+file.getName()+".");
				} else {
					formatter.format( "\t" + parent+file.getName());
				}
			}
		} else {
			formatter.format("\t" + folder.getName() + " DIRECTORY DOES NOT EXIST");
		}
	}

	public void compileAndRunContents(String targetFolderName, Compiler compiler, Executer executer,
			Map<String, List<String[]>> arguments, FileFilter filterSource, FileFilter filterExecutables) throws IOException {
		Formatter formatter = null;
		try {
			formatter = new Formatter(new FileWriter(targetFolderName + "_CompRunResults.txt"));
			for (Student st : students) {
				formatter.format(st.getId());
				logger.debug("Compiling files for " + st.getId());
				try {
					if (st.getRepo() == null) {
						formatter.format("\tNO REPOSITORY\n");
					} else {
						File rootFolder = st.getRepo().getRootFolder(targetFolderName);
						File[] files = st.getRepo().getContentsRecursively(targetFolderName, filterSource).toArray(new File[1]);
						if (files != null) {
							try {
								List<String> errors = compiler.compile(files);
								for (String err : errors) {
									formatter.format("\n\t\t" + err);
								}
							}catch(Exception ex) {
								formatter.format("\tError while compiling\n");
								throw ex;
							}
							
							List<String> executables = st.getRepo().getExecutablesRecursively(rootFolder, filterExecutables);

							if (executables.size() > 0) { //duzeltilecek
								for (String executable : executables) {
									List<String[]> argsList = null;
									if (arguments != null) {
										argsList = arguments.get(executable);
										if (argsList == null) {
											argsList = arguments.get("*");
										}
									}
									if (argsList != null) {
										for (String[] args : argsList) {
											formatter.format("\n\t\t Args:" + Arrays.asList(args));
											String result = executer.execute(rootFolder,executable, args);
											formatter.format("\n\t\t Output: " + result);
										}

									} else {
										String[] args = {};
										String result = executer.execute(rootFolder,executable, args);
										result = result.replace("\n", "\n\t\t\t\t");
										formatter.format("\n\t\t Output: " + result);
									}
								}
							}
						} else {
							formatter.format("\tNO " + targetFolderName);
						}
						formatter.format("\n");
					}
				} catch (Exception ex) {
					logger.error("Error while processing student " + st.getId(), ex);
				}
			}
		} finally {
			if (formatter != null) {
				formatter.close();
			}
		}
	}

}
