package tr.edu.mu.ceng.homeworkanalyzer;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface Compiler {

	public List<String> compile(File[] files) throws IOException;
	
	public List<String> compile(File file) throws IOException;
	
}
